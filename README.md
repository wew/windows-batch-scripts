# README #

This is a collection of useful and specialized batch scripts that I created and accumulated over the years.

I do not take full credit for creating them as I obtained most scripts in small chunks from the web and heavily adapted them to suit my personal needs. Having said that, you will not find any such complete scripts elsewhere, because as mentioned, their functions are very specific in nature, as opposed to being generic.

All the scripts were tested on Windows 7. They should run on other versions as well, but there are no guarantees. Please make sure you test them in a safe environment (create a new test folder or use a virtual machine) before utilizing them for daily use.

### What is this repository for? ###

* It is for the automation of MozBackup that allows full, hassle-free backup of Firefox and/or Thunderbird.

### How do I get set up? ###

* Note to self: Add setup steps here.

### License ###

* Licensed under The MIT License.