:: FF_TB_Backup.bat
:: The name of the file "FF_TB_Backup" means Firefox_Thunderbird_Backup.
:: The role of this file is to call the other two batch files and execute them.
:: This means the use of this file is optional. You do not need to run it if you
:: want to backup only one of the programs (either Firefox or Thunderbird only).



:: STEP 1.
@echo off
:: Set Command Prompt's color to black background and grey text.
COLOR 08
@echo off
:: Set the title of the Command Prompt window and display a welcome message.
TITLE Firefox and Thunderbird Automatic Backup with MozBackup
@echo Welcome to Firefox and Thunderbird Automated Backup!



:: STEP 2 (OPTIONAL).
@echo Opening target folder...
:: Open the folder where Mozbackup will be creating the backup files.
:: Modify the output on line 5 in both Default.ffprofile and Default.tbprofile files.
:: Example: C:\Thunderbird Backups. Notice how no quotes are needed in *.*profile files.
explorer "D:\Mozilla Products Backups"



:: STEP 3.
:: Call the two batch files for execution.
:: Modify the following paths to where you placed the two batch files.
CALL "D:\MozBackup Scripts\Firefox_Backup.bat"
CALL "D:\MozBackup Scripts\Thunderbird_Backup.bat"