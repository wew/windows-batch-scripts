:: Thunderbird_Backup.bat
:: This batch file does all of the following:
:: 1. Deletes Thunderbird backup file(s) older than 7 days.
:: 2. Kills thunderbird.exe process if it is running. Ignores it if it is not running.
:: 3. Runs MozBackup.exe with the pre-defined config file Default.thunderbirdprofile
:: 4. Optional: Re-launches Thunderbird as soon as MozBackup is finished.



:: STEP 1.
@echo off
:: Set Command Prompt's color to black background and grey text.
COLOR 08
@echo off
:: Print message.
@echo Thunderbird backup will begin shortly!



:: STEP 2.
:: Loop through the directory non-recursively, and list all the old files that will be deleted in STEP 3.
:: Modify the following path to where your file(s) exist.
:: Example: "C:\Thunderbird Backups". Do NOT remove the quotes surrounding the path.
FORFILES /p "D:\Mozilla Products Backups" /M Thunderbird*.pcv /C "cmd /c echo @file was created on @fdate (over a week ago) and will be removed" /D -8
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 3.
:: Print message.
echo.
@echo Deleting old Thunderbird backup file(s)...
@echo off
:: Remove files older than 7 days and display a success message.
:: NOTE: Files are permanently deleted. They do NOT go to the recycle bin first.
:: If you don't have any .pcv backup file(s) yet, or have no files older than 7 days,
:: then this command will be automatically ignored.
:: Modify the following path to where your file(s) exist.
:: Example: "C:\Thunderbird Backups". Do NOT remove the quotes surrounding the path.
FORFILES /p "D:\Mozilla Products Backups" /M Thunderbird*.pcv /C "cmd /c Del @path echo @fname & cmd /c echo Successfully deleted @file" /D -8
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 4.
:: Print message.
echo.
@echo off
@echo Thunderbird is quitting...
:: Kill thunderbird.exe process if it is running. Ignore it if it is not running.
:: This is important because MozBackup will not start unless Thunderbird is closed.
:: You should NOT need to change anything below.
@echo off
@echo off tasklist /nh /fi "imagename eq thunderbird.exe" | find /i "thunderbird.exe" >nul && ( taskkill /f /im thunderbird.exe /T )
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 5.
:: Print message.
echo.
@echo Thunderbird backup is in progress...
:: Run MozBackup with the pre-defined configuration file "Default.thunderbirdprofile"
:: Modify the following path to where you placed the "Default.thunderbirdprofile" file.
:: Example: "C:\Default.thunderbirdprofile".
START /W "" "C:\Program Files\MozBackup\MozBackup.exe" "D:\Batch\MozBackup\Default.thunderbirdprofile"
:: Print message.
@echo Thunderbird backup finished!
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 6 (OPTIONAL).
:: Uncomment the following lines by removing the double colons if you'd
:: like Thunderbird to automatically re-launch after the backup is finished.
:: Modify the path to where thunderbird.exe is located in your system.
:: Example: "C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe".
:: Do NOT remove the quotes surrounding the path.
:: echo.
:: @echo Thunderbird backup completed successfully! Re-launching Thunderbird...
:: echo.
:: START /W /B "" "C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe"
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
:: TIMEOUT /T 10 /NOBREAK