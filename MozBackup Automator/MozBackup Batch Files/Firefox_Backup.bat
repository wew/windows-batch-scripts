:: Firefox_Backup.bat
:: This batch file does all of the following:
:: 1. Deletes Firefox backup file(s) older than 7 days.
:: 2. Kills firefox.exe process if it is running. Ignores it if it is not running.
:: 3. Runs MozBackup.exe with the pre-defined config file Default.firefoxprofile
:: 4. Optional: Re-launches Firefox as soon as MozBackup is finished.



:: STEP 1.
@echo off
:: Set Command Prompt's color to black background and grey text.
COLOR 08
@echo off
:: Print message.
@echo Firefox backup will begin shortly!



:: STEP 2.
:: Loop through the directory non-recursively, and list all the old files that will be deleted in STEP 3.
:: Modify the following path to where your file(s) exist.
:: Example: "C:\Firefox Backups". Do NOT remove the quotes surrounding the path.
FORFILES /p "D:\Mozilla Products Backups" /M Firefox*.pcv /C "cmd /c echo @file was created on @fdate (over a week ago) and will be removed" /D -8
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 3.
:: Print message.
echo.
@echo Deleting old Firefox backup file(s)...
@echo off
:: Remove files older than 7 days and display a success message.
:: NOTE: Files are permanently deleted. They do NOT go to the recycle bin first.
:: If you don't have any .pcv backup file(s) yet, or have no files older than 7 days,
:: then this command will be automatically ignored.
:: Modify the following path to where your file(s) exist.
:: Example: "C:\Firefox Backups". Do NOT remove the quotes surrounding the path.
FORFILES /p "D:\Mozilla Products Backups" /M Firefox*.pcv /C "cmd /c Del @path echo @fname & cmd /c echo Successfully deleted @file" /D -8
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 4.
:: Print message.
echo.
@echo off
@echo Firefox is quitting...
:: Kill firefox.exe process if it is running. Ignore it if it is not running.
:: This is important because MozBackup will not start unless Firefox is closed.
:: You should NOT need to change anything below.
@echo off
@echo off tasklist /nh /fi "imagename eq firefox.exe" | find /i "firefox.exe" >nul && ( taskkill /f /im firefox.exe /T )
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 5.
:: Print message.
echo.
@echo Firefox backup is in progress...
:: Run MozBackup with the pre-defined configuration file "Default.firefoxprofile"
:: Modify the following path to where you placed the "Default.firefoxprofile" file.
:: Example: "C:\Default.firefoxprofile".
START /W "" "C:\Program Files\MozBackup\MozBackup.exe" "D:\Batch\MozBackup\Default.firefoxprofile"
:: Print message.
@echo Firefox backup finished!
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
TIMEOUT /T 10 /NOBREAK



:: STEP 6 (OPTIONAL).
:: Uncomment the following lines by removing the double colons if you'd
:: like Firefox to automatically re-launch after the backup is finished.
:: Modify the path to where firefox.exe is located in your system.
:: Example: "C:\Program Files (x86)\Mozilla Firefox\firefox.exe".
:: Do NOT remove the quotes surrounding the path.
:: echo.
:: @echo Firefox backup completed successfully! Re-launching Firefox...
:: echo.
:: START /W /B "" "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
:: 10-second timer for you to have enough time to read the output. Modify or completely remove it.
:: TIMEOUT /T 10 /NOBREAK